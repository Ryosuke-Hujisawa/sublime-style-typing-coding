# sublime-style-typing-coding

This is sublime style typing coding animation based css, html and JavaScript 


## config


```html
<pre id="typewriter">
<span class="var-highlight">var</span> object = {
    name: <span class="string-highlight">'Ryosuke Hujisawa'</span>,
    type: <span class="string-highlight">'Full-Stack Developer'</span>,
    location: <span class="string-highlight">'Earth'</span>,
    properties:[<span class="string-highlight">'JavaScript'</span>,
                <span class="string-highlight">'HTML'</span>,
                <span class="string-highlight">'CSS'</span>];
}; </pre>
```

## video

![](http://wordtranslate.info/img/sublime_style_coding_animation.gif)

## Do you wanna try ?

Have try visit my site or my codepen !

https://ryosuke-hujisawa.github.io/

https://codepen.io/hujisawa/pen/JBmEOK